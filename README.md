# About DeepDTA: deep drug-target binding affinity prediction

The approach used in this work is the modeling of protein sequences and compound 1D representations (SMILES) with convolutional neural networks (CNNs) to predict the binding affinity value of drug-target pairs.

![Figure](https://github.com/hkmztrk/DeepDTA/blob/master/docs/figures/deepdta.PNG)
# Installation

## Environment
An optional and efficient way to build the model is through the Google Cloud Platform- Notebook instances which can use a graphics processing unit (GPU). 
Use the existed notebook 'pomicell-tensrflow-gpu-notebook-instance' or generate a new virtual machine instance with the following settings:

* framework: TensorFlow Enterprise 1.15 -> with 1 NVIDIA Tesla T4
* Region: europe-west 1 (Belgium)
* Zone: europe-west1-b
* Operating System: Debian 9
* Environment: TensorFlow Enterprise 1.15 (with Intel� MKL-DNN/MKL)
* Machine Type: n1-standard-4 (4 vCPUs, 15 GB RAM)
* GPU type: NVIDIA Tesla K80
* Number of GPUs: 1

* install-nvidia-driver: True (mark V next to 'Install NVIDIA GPU driver authomatically for me')


## Data

Choose the desired dataset Kiba / Davis and update the relevant field under the --dataset_path flag when running.
Please see the [readme](https://github.com/hkmztrk/DeepDTA/blob/master/data/README.md) for detailed explanation about the generation of these data files and how to generate your own dataset.

## Requirements

Using the above Google Cloud Platform Notebook instance with Tensorflow framework, the following packages will be authomatically installed in your machine, and are required to run the code:

*  [Python 3.6](https://www.python.org/downloads/)
*  [Keras 2.3.1](https://pypi.org/project/Keras/)
*  [Tensorflow 1.15.3](https://www.tensorflow.org/install/)
*  Tensorboard 1.15.0
*  numpy 1.19.2
*  matplotlib

Please manually install an additional package:

* bioservices 1.7.9

Make sure the "data" folder is placed under the "source" directory. 

# Usage- Build the DeepDTA model

We recommend using SSH, with a tmux shell to enable follow up of the progress.

cd into /home/jupyter/DeepDTA/source directory

run the following code, specifying the desired parameters, and defining the desired log_dir where the output will be written (the log_dir will be created under the source/ directory if doesn't exist).




```




python run_experiments.py --num_windows 32 \
                          --seq_window_lengths 8 12 \
                          --smi_window_lengths 4 8 \
                          --batch_size 256 \
                          --num_epoch 100 \
                          --max_seq_len 1000 \
                          --max_smi_len 100 \
                          --dataset_path 'data/kiba/' \
                          --problem_type 1 \
                          --log_dir 'logs/'




```

The model with best performaces (loss (MSE) and CI score) will be saved in the models/ directory, named by the creation date and the parameters values that emearged the best performances.


**For citation:**

```
@article{ozturk2018deepdta,
  title={DeepDTA: deep drug--target binding affinity prediction},
  author={{\"O}zt{\"u}rk, Hakime and {\"O}zg{\"u}r, Arzucan and Ozkirimli, Elif},
  journal={Bioinformatics},
  volume={34},
  number={17},
  pages={i821--i829},
  year={2018},
  publisher={Oxford University Press}
}
```
