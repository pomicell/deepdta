Namespace(batch_size=256, binary_th=0.0, checkpoint_path='', dataset_path='data/kiba/', is_log=0, learning_rate=0.001, log_dir='logs_with_model_100_epoch/1603885056.671529/', max_seq_len=1000, max_smi_len=100, num_classes=0, num_epoch=100, num_hidden=0, num_windows=[32], problem_type=1, seq_window_lengths=[8, 12], smi_window_lengths=[4, 8])
---Parameter Search-----
P1 = 0,  P2 = 0, P3 = 0, Fold = 0, CI-i = 0.856863, CI-ii = 0.857071, MSE = 0.198267
P1 = 0,  P2 = 0, P3 = 1, Fold = 0, CI-i = 0.856121, CI-ii = 0.855507, MSE = 0.205739
P1 = 0,  P2 = 1, P3 = 0, Fold = 0, CI-i = 0.867156, CI-ii = 0.865457, MSE = 0.190349
P1 = 0,  P2 = 1, P3 = 1, Fold = 0, CI-i = 0.866297, CI-ii = 0.865116, MSE = 0.195491
P1 = 0,  P2 = 0, P3 = 0, Fold = 1, CI-i = 0.857275, CI-ii = 0.857971, MSE = 0.199619
P1 = 0,  P2 = 0, P3 = 1, Fold = 1, CI-i = 0.858185, CI-ii = 0.857573, MSE = 0.199231
P1 = 0,  P2 = 1, P3 = 0, Fold = 1, CI-i = 0.865362, CI-ii = 0.865027, MSE = 0.196231
P1 = 0,  P2 = 1, P3 = 1, Fold = 1, CI-i = 0.864729, CI-ii = 0.864275, MSE = 0.199151
P1 = 0,  P2 = 0, P3 = 0, Fold = 2, CI-i = 0.856713, CI-ii = 0.856781, MSE = 0.205871
P1 = 0,  P2 = 0, P3 = 1, Fold = 2, CI-i = 0.853628, CI-ii = 0.854505, MSE = 0.211394
P1 = 0,  P2 = 1, P3 = 0, Fold = 2, CI-i = 0.857334, CI-ii = 0.858956, MSE = 0.209283
P1 = 0,  P2 = 1, P3 = 1, Fold = 2, CI-i = 0.858659, CI-ii = 0.859352, MSE = 0.207217
P1 = 0,  P2 = 0, P3 = 0, Fold = 3, CI-i = 0.856486, CI-ii = 0.855045, MSE = 0.219255
P1 = 0,  P2 = 0, P3 = 1, Fold = 3, CI-i = 0.859497, CI-ii = 0.857683, MSE = 0.209799
P1 = 0,  P2 = 1, P3 = 0, Fold = 3, CI-i = 0.863383, CI-ii = 0.862513, MSE = 0.225986
P1 = 0,  P2 = 1, P3 = 1, Fold = 3, CI-i = 0.862993, CI-ii = 0.863056, MSE = 0.203761
P1 = 0,  P2 = 0, P3 = 0, Fold = 4, CI-i = 0.857066, CI-ii = 0.856957, MSE = 0.201648
P1 = 0,  P2 = 0, P3 = 1, Fold = 4, CI-i = 0.854700, CI-ii = 0.852668, MSE = 0.203217
P1 = 0,  P2 = 1, P3 = 0, Fold = 4, CI-i = 0.864345, CI-ii = 0.864095, MSE = 0.202406
P1 = 0,  P2 = 1, P3 = 1, Fold = 4, CI-i = 0.862656, CI-ii = 0.860581, MSE = 0.203624
Building model out of best params: P1 (num_w) = 32,  P2 (smi_w_length) = 8, P3 (seq_w_length) = 8
Evaluation Results of Best Model (on test set): CI-i = 0.872607, CI-ii = 0.871792, MSE = 0.175893
Validation Performance CI
[0.8671561305028485, 0.8653619968198389, 0.8573342769723781, 0.8633833743189371, 0.864345015766419]
Validation Performance MSE
[0.1903493307983381, 0.19623119361179595, 0.20928311765759472, 0.22598638097362508, 0.20240627648599482]
Setting 1
validation_avg_perf = 0.86352,  validation_avg_mse = 0.20485, validation_std = 0.00333
